﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MapPlanLib.Models;
using MapPlanLib.Services;

namespace MapPlanner.Controllers
{
    public class HomeController : Controller
    {
        IUploadService uploadService = new UploadService();

        public ActionResult Index()
        {
            var model = new AllUploads();
            model.Uploads = uploadService.GetAllUploads();
            return View(model);

        }
        [HttpPost]
        public ActionResult IndexBG(string url)
        {
            var model = new AllUploads();
            model.Uploads = uploadService.GetAllUploads();
            TempData["URL"] = url;
            return RedirectToAction("Index");
        }
        public PartialViewResult Search(string Tags, string cat)
        {
            var tags = Tags.Split(' ').ToList();
            var model = new AllUploads();
            if (tags.Count == 1 && tags[0] == "")
            {
                model.Uploads = uploadService.GetAllUploads();
            }
            else
            {
                model.Uploads = uploadService.GetUploadsByTags(tags);
            }

            category category = new category();

            if (cat == "icon")
            {
                category = category.ICON;
            }
            else if (cat == "map")
            {
                category = category.MAP;
            };

            model.Uploads = model.Uploads.Where(x => x.CATEGORY == category).ToList();
            

            return PartialView("_Search", model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Delete(int id)
        {
            uploadService.DeleteUpload(id);
            return RedirectToAction("Index");
        }

        public ActionResult Upload()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Upload(UploadModel upload)
        {
            if ((System.Web.HttpContext.Current.User != null) && System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
            {
                upload.USERNAME = User.Identity.Name;
            }
            uploadService.AddNewUpload(upload);
            return RedirectToAction("Index");
        }
    }
}

//<div class="col-sm-30 col-md-20">
//    <li>
//        @using(Html.BeginForm("IndexBG", "Home"))
//        {
//            <button type = "submit" value="@upload.IMG_URL" name="url">
//                <a href = "~/Home/Index/" class="thumbnail">
//                    <img src = "@upload.IMG_URL" >
//                </ a >
//            </ button >
//        }
//        <a href = "~/Home/Delete/@upload.IMG_ID" >< input type="button" value="Delete" name="delete" /></a>
//    </li>
//</div>