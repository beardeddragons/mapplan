﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MapPlanner.Startup))]
namespace MapPlanner
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
