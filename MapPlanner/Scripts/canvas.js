﻿var context = document.getElementById('canvas').getContext("2d");
document.getElementById("clearCanvas").onclick = function () { clear() };

document.getElementById("penButton").onclick = function () { pn() };
document.getElementById("textButton").onclick = function () { txt() };
document.getElementById("rectangleButton").onclick = function () { rect() };

document.getElementById("yellowButton").onclick = function () { yellow() };
document.getElementById("redButton").onclick = function () { red() };
document.getElementById("greenButton").onclick = function () { green() };
document.getElementById("blueButton").onclick = function () { blue() };
document.getElementById("blackButton").onclick = function () { black() };
document.getElementById("whiteButton").onclick = function () { white() };

document.getElementById("undoButton").onclick = function () { undo() };

//document.getElementById("color").onclick = function () { changeColor(document.getElementById("color").value) };

document.getElementById("download").onclick = function () { download() };

document.getElementById("scale").onclick = function () { setSize(document.getElementById("scale").value) };

var saver;
var size;
var textLength = 0;
var firstClick = true;
var tool = 1;
var imgUrl;

$('#canvas').mousedown(function (e) {
    var mouseX = e.pageX - this.offsetLeft;
    var mouseY = e.pageY - this.offsetTop;

    paint = true;
    if (tool == 1) {
        Everything.push(new line(mouseX, mouseY, false));
    } else if (tool == 2) {
        Everything.push(new text(mouseX, mouseY, document.getElementById("textBox").value));
    }else if (tool == 3) {
        Everything.push(new rectangle(mouseX, mouseY, mouseX, mouseY));
    } else if (tool == 4) {
        console.log("clicked an img")
        Everything.push(new img(imgUrl, mouseX, mouseY));
        console.log(Everything);
    }
    else {
        addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop);
    }

    redraw();
});

$('#canvas').mousemove(function (e) {
    var mouseX = e.pageX - this.offsetLeft;
    var mouseY = e.pageY - this.offsetTop;

    if (paint) {
        if (tool == 1) {
            var lastIndex = Everything.length - 1;

            Everything[lastIndex].drag = true;
            Everything[lastIndex].x.push(mouseX);
            Everything[lastIndex].y.push(mouseY);
        } else if (tool == 2) {

        } else if (tool == 3) {
            var lastIndex = Everything.length - 1;

            if (Everything[lastIndex].startX <= e.pageX - this.offsetLeft) {
                Everything[lastIndex].minX = Everything[lastIndex].startX;
            }
            else {
                Everything[lastIndex].minX = e.pageX - this.offsetLeft;
            }

            if (Everything[lastIndex].startY <= e.pageY - this.offsetTop) {
                Everything[lastIndex].minY = Everything[lastIndex].startY;
            }
            else {
                Everything[lastIndex].minY = e.pageY - this.offsetTop;
            }

            Everything[lastIndex].width = Math.abs(Everything[lastIndex].startX - (e.pageX - this.offsetLeft));
            Everything[lastIndex].height = Math.abs(Everything[lastIndex].startY - (e.pageY - this.offsetTop));
        }
        else {
            addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop, true);
        }
        redraw();
    }
});

$('#canvas').mouseup(function (e) {
    paint = false;
});

function download() {
    var dataURL = canvas.toDataURL('image/png');
    document.getElementById('download').href = dataURL;
}

function changeColor(color) {
    curColor = color;
}

function red() {
    curColor = colorRed;
    console.log("red");
}

function green() {
    curColor = colorGreen;
    console.log("green");
}

function yellow() {
    curColor = colorYellow;
    console.log("yellow");
}

function blue() {
    curColor = colorBlue;
    console.log("blue");
}

function black() {
    curColor = colorBlack;
    console.log("black");
}

function white() {
    curColor = colorWhite;
    console.log("white");
}

function pn() {
    tool = 1;
    console.log(tool);
}

function txt() {
    tool = 2;
    console.log(tool);
}

function rect() {
    tool = 3;
    console.log(tool);
}

function iconClick(url) {
    tool = 4;

    imgUrl = url;
    //var image = new Image();
    //image.src = url;

    //image.onload = function () {
    //    context.drawImage(image, 100, 100, 50, 50);
    //};
}

function setSize(slider) {
    curSize = slider;
    console.log("changed");
}
var paint;

var colorRed = "#ff0000";
var colorGreen = "#00ff00";
var colorYellow = "#ffff00";
var colorBlue = "#0000ff";
var colorBlack = "#000000";
var colorWhite = "#ffffff";

var curColor = colorRed;

var curSize = 5;

var Everything = [];

function rectangle(startX, startY, endX, endY) {
    console.log("new rectangle");

    this.id = 3;

    this.startX = startX;
    this.startY = startY;

    this.color = curColor;

    if (startX <= endX) {
        this.minX = startX;
    }
    else {
        this.minX = endX;
    }

    if (startY <= endY) {
        this.minY = startY;
    }
    else {
        this.minY = endY;
    }

    this.width = Math.abs(endX - startX);
    this.height = Math.abs(endY - startY);
    this.show = true;

    this.lineWidth = curSize;
}

function text(x,y,val) {
    console.log("new text");

    this.id = 2;

    this.x = x;
    this.y = y;

    this.value = val;

    this.show = true;

    this.color = curColor;
    this.size = curSize;
}

function line(xCord, yCord, dragging) {
    console.log("new line");

    this.id = 1;

    this.x = [];
    this.y = [];

    this.x.push(xCord);
    this.y.push(yCord);

    this.drag = dragging;

    this.show = true;
    this.color = curColor;
    this.size = curSize;
}

function img(url, x, y) {
    console.log("Made a new img with url " + url);

    this.id = 4;

    this.url = url;

    this.x = x;
    this.y = y;

    this.show = true;
}

function addClick(x, y, dragging) {
    if (tool == 1) {
        Everything.push(new line(x,y,dragging));
    } else if(tool == 2){
        if (!dragging) {
            Everything.push(new text(x, y, document.getElementById("textBox").value));
        }
    }
}

function print(text) {
    console.log(text);
}

function undo() {
    Everything.pop();
    redraw();
}

function redraw() {
    context.clearRect(0, 0, context.canvas.width, context.canvas.height);

    context.lineJoin = "round";

    for (var i = 0; i < Everything.length; i++) {
        if (Everything[i].show) {
            if (Everything[i].id == 1) {
                context.beginPath();
                for (var j = 0; j < Everything[i].x.length; j++){
                    if (Everything[i].drag) {
                        context.moveTo(Everything[i].x[j - 1], Everything[i].y[j - 1]);
                    } else {
                        context.moveTo(Everything[i].x[j] - 1, Everything[i].y[j]);
                    }
                    context.lineTo(Everything[i].x[j], Everything[i].y[j]);
                    context.closePath();
                    context.strokeStyle = Everything[i].color;
                    context.lineWidth = Everything[i].size;
                    context.stroke();
                }
            } else if (Everything[i].id == 2) {
                context.fillStyle = Everything[i].color;
                context.font = "bold " + Everything[i].size * 5 + "px Arial";
                context.fillText(Everything[i].val, Everything[i].x, Everything[i].y);
            } else if (Everything[i].id == 3) {
                context.globalAlpha = .3;
                context.beginPath();
                context.fillStyle = Everything[i].color;
                context.lineWidth = Everything[i].lineWidth;
                context.fillRect(Everything[i].minX, Everything[i].minY, Everything[i].width, Everything[i].height);
                context.stroke();
                context.globalAlpha = 1;
            } else if (Everything[i].id == 4) {

                console.log(JSON.stringify(Everything[i]));
                console.log(Everything[i].x);
                console.log(Everything[i].y);

                var drawX = Everything[i].x;
                var drawY = Everything[i].y;

                console.log(drawX);
                console.log(drawY);

                var image = new Image();
                image.src = Everything[i].url;

                console.log("Image loaded: " + image.src + " at " + drawX + "," + drawY);
                context.drawImage(image, drawX, drawY, 50, 50);
            }
        }
    }
}

function clear() {
    context.clearRect(0, 0, context.canvas.width, context.canvas.height);
    console.log("clicked");
    for (var i = 0; i < X.length; i++){
        Everything.pop();
    }
}