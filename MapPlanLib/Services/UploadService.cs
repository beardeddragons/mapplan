﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MapPlanLib.Models;
using MapPlanEF;
using System.IO;
using System.Web.Security;

namespace MapPlanLib.Services
{
    public class UploadService : IUploadService
    {
        MapOutEntities context = new MapOutEntities();
        private readonly object from;

        public void AddNewUpload(UploadModel upload)
        {
            UPLOAD dbUpload = new UPLOAD
            {
                IMG_URL = upload.IMG_URL,
                USERNAME = upload.USERNAME,
                ISPRIVATE = upload.ISPRIVATE,
                CATEGORY = upload.CATEGORY.ToString(),
            };
            int i = 0;
            var arrayOfTags = upload.TAGS[0].Split('\n');
            foreach (string tag in arrayOfTags)
            {
                IMG_TAGS dbImgTags = new IMG_TAGS
                {
                    TAG_NAME = arrayOfTags[i].ToString().Trim()
                };
                context.IMG_TAGS.Add(dbImgTags);
                i++;
            };
            context.UPLOADS.Add(dbUpload);
            context.SaveChanges();
        }

        public void DeleteUpload(int ID)
        {
            var upload = context.UPLOADS.Single(x => x.IMG_ID == ID); //new UPLOAD()
                                                                      //{
                                                                      // IMG_ID = ID
                                                                      //};

            var tags = context.IMG_TAGS.Where(x => x.IMG_ID == ID);

            foreach (var tag in tags)
            {
                context.IMG_TAGS.Attach(tag);
                context.IMG_TAGS.Remove(tag);
            }

            context.UPLOADS.Attach(upload);
            context.UPLOADS.Remove(upload);
            context.SaveChanges();
        }

        public List<UploadModel> GetAllUploads()
        {
            var name = System.Web.HttpContext.Current.User.Identity.Name;
            var uploads = context.UPLOADS.Select(x => x).ToList().Select(
            dh =>
                new UploadModel
                {
                    IMG_ID = dh.IMG_ID,
                    IMG_URL = dh.IMG_URL,
                    USERNAME = dh.USERNAME,
                    ISPRIVATE = dh.ISPRIVATE,
                    CATEGORY = (category)Enum.Parse(typeof(category), dh.CATEGORY),
                    TAGS = context.IMG_TAGS.Where(
                        e => e.IMG_ID == dh.IMG_ID
                    ).Select(t =>
                        t.TAG_NAME
                    ).ToList()
                }).Where(x => x.ISPRIVATE == false || (x.ISPRIVATE == true && x.USERNAME == name));
            return uploads.ToList();
        }


        public UploadModel GetUploadById(int id)
        {
            return GetAllUploads().Single(x => x.IMG_ID == id);
        }

        //TODO: "Make a loop!" --Baret Woods-
        public List<UploadModel> GetUploadsByTags(List<string> tags)
        {
            List<UploadModel> results = new List<UploadModel>();

            foreach (UploadModel upload in GetAllUploads())
            {
                foreach (string tag in tags)
                {
                    tag.Trim();
                }
                var excepted = tags.Except(upload.TAGS).ToList();
                System.Diagnostics.Debug.WriteLine("excepted amount: " + excepted.Count);
                if(excepted.Count == 0)
                {
                    results.Add(upload);
                }
            }

            return results;
        }
    }
}
