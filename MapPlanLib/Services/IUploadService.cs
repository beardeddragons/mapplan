﻿using MapPlanLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapPlanLib.Services
{
    public interface IUploadService
    {
        List<UploadModel> GetAllUploads();
        UploadModel GetUploadById(int id);
        void AddNewUpload(UploadModel upload);
        void DeleteUpload(int ID);
        List<UploadModel> GetUploadsByTags(List<string> tags);
    }
}
