﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapPlanLib.Models
{
    public enum category
    {
        MAP,
        ICON,
    };
    public class UploadModel
    {

        public int IMG_ID { get; set; }
        public string IMG_URL { get; set; }
        public string USERNAME { get; set; }
        public bool ISPRIVATE { get; set; }
        public category CATEGORY { get; set; }
        public List<string> TAGS { get; set; }
    }
}
