﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapPlanLib.Models
{
    public class AllUploads
    {
        public IList<UploadModel> Uploads { get; set; }
    }
}
